public class Person {
    private double weight;
    private double height;
    private String name;
    Weight unitWeight;
    Height unitHeight;
    
    public Person(String name, double weight, Weight unitWeight, double height, Height unitHeight){
        this.name = name;
        this.weight = weight;
        this.unitWeight = unitWeight;
        this.height = height;
        this.unitHeight = unitHeight;
    }
    
    public String getName(){
        return name;
    }
    
    public double getBMI(){
        double weightToK = weight / 2.205;
        double heightToM = height / 39.37;

        if(unitWeight == Weight.K && unitHeight == Height.M){
            return weight / (Math.pow(height, 2));
        } else if(unitWeight == Weight.LB && unitHeight == Height.M){
            return weightToK / (Math.pow(height, 2));
        } else if(unitWeight == Weight.K && unitHeight == Height.IN){
            return weight / (Math.pow(heightToM, 2));
        } else{
            return weightToK / (Math.pow(heightToM, 2));
        }
    }
    
    public double getWeight(){
        return this.weight;
    }
    
    public void setWeight(double weight){
        this.weight = weight;
    }
    
    public double getWeightLbs(){
        return this.weight;
    }
    
    public double getHeightInches(){
        return this.height;
    }
}
